import scipy.io as sio
import numpy as np
from math import sqrt
import matplotlib.pyplot as plt
import Trial as cd

import resource

resource.setrlimit(
    resource.RLIMIT_CORE,
    (resource.RLIM_INFINITY, resource.RLIM_INFINITY))

def fit_linreg(X, yy, alpha=10):

	N = X.shape[0]
	D = X.shape[1]

	#Add one more column to the Design Matrix, to insert the bias 

	X_bias = np.concatenate([np.ones((X.shape[0],1)), X], axis=1) 


	#Create an Identity Matrix add added to the X_bias so regularization constant is added.

	identinty_matrix = np.eye(D+1)
	identinty_matrix = sqrt(alpha) * identinty_matrix
	identinty_matrix[0,:] = 0
	X_regularized  = np.concatenate((X_bias,identinty_matrix),axis=0)
	
	# Add a D vector to the outputs prediction, for regularization purposes
	y_regularized = np.append(yy,np.zeros(D+1))

	weights  = np.linalg.lstsq(X_regularized, y_regularized,rcond=0)[0]
	bias = weights[0]
	weights = weights[1:]

	# Root Square Mean error 
	y_pred = np.dot(X,weights) + bias
	error  = sqrt( ((y_pred - yy)**2).mean() )	

	return weights,bias,error

def sigmoid_function(X,weights,bias):
	y_pred = np.dot(X,weights) + b 
	sigma = 1/(1 + np.exp(-y_pred))
	return sigma



def compute_error(X,yy,weights,bias):
	y_pred = np.dot(X,weights) +bias
	error  = sqrt( ((y_pred - yy)**2).mean() )	
	return error

def k_feature(X,k):
	return X[:,k-1]

file_read = sio.loadmat("ct_data.mat",squeeze_me=True)
x_train = file_read["X_train"]
x_val  = file_read["X_val"]
x_test = file_read["X_test"]

y_train = file_read["y_train"]
y_val = file_read["y_val"]
y_test = file_read["y_test"]


def fit_neural_network( weight_fitted,bias,ww_sigmoidal,bias_sigmoidal,x_train,y_train,x_val,y_val,alpha=10):

	learning_rate = 0.0000055	
	epochs = 0 
	#If the System's Validation error doesn't drop for 20 epochs, stop the algorithms
	early_stopping_counter = 0;
	minValidationError = None
	while(epochs < 2500):
		Error,(ww_bar, bb_bar, V_bar, bk_bar) = cd.nn_cost((weight_fitted,bias,ww_sigmoidal,bias_sigmoidal), x_train, y_train, alpha)
		weight_fitted -= learning_rate*ww_bar
		bias -= learning_rate*bb_bar
		ww_sigmoidal -= learning_rate*V_bar
		bias_sigmoidal -= learning_rate*bk_bar
		valError,_ = cd.nn_cost((weight_fitted,bias,ww_sigmoidal,bias_sigmoidal), x_val, y_val,alpha)
		if(minValidationError == None or minValidationError > valError):
			early_stopping_counter = 0
			minValidationError = valError
		else:
			early_stopping_counter+=1
		if(early_stopping_counter == 20):
			break;
		epochs +=1		
	return Error


# Question 1

# Q1a 
y_train_mean = np.mean(y_train)

# Mean of y_val with its standard error 

y_val_mean = np.mean(y_val)
y_len = len(y_val)
y_val_var = np.var(y_val)

var_y_mean = sqrt(y_val_var/y_len) 
# print("Validation SE:",y_val_mean-var_y_mean,"To:",y_val_mean+var_y_mean)

# Mean of the first elements 5785 elements of y_train
size = 5785
y_train_mean_size = np.mean(y_train[:size])
y_train_var_size = np.var(y_train[:size])
y_train_var_of_mean = sqrt(y_train_var_size/size)
# print("Training SE:",y_train_mean_size-y_train_var_of_mean,"To:",y_train_mean_size+y_train_var_of_mean)

#Q1b
# # Items that exactly the same if +1 * -1 will actually sum up to 0 
# x_train = np.unique(x_train,axis=1)


x_mul = np.ones((x_train.shape[0],1))
x_mul[0::2] = -1

x_train_new = x_train*x_mul
x_vector = x_train_new.sum(0)

indexes = np.where(x_vector ==0)[0]
x_train = np.delete(x_train,np.where(x_vector==0),axis=1)
x_val = np.delete(x_val,np.where(x_vector==0),axis=1)
x_test = np.delete(x_test,np.where(x_vector==0),axis=1)


_,train_ind= np.unique(x_train,axis=1,return_index=True)
train_ind.sort()
x_train = x_train[:,train_ind]
x_val = x_val[:,train_ind]
x_test = x_test[:,train_ind]


#Question 2 ------------------------------------------------------------------------------------------------------------------#

# weight_fitted,bias,error = fit_linreg(x_train,y_train)
# weights_fitted_gradient,b = cd.fit_linreg_gradopt(x_train ,y_train,alpha=10)

# y_pred = np.dot(x_train,weights_fitted_gradient)+b
# error_gradient = sqrt(((y_pred - y_train)**2).mean())

# print("Train error: ",error,error_gradient)

# print(error - error_gradient)

# compute_error_leastSquares = compute_error(x_val,y_val,weight_fitted,bias);
# compute_error_gradient = compute_error(x_val,y_val,weights_fitted_gradient,b)
# print("Validation error: ",compute_error_leastSquares,compute_error_gradient)


# #Question 3 ------------------------------------------------------------------------------------------------------------------#

# #Q3a

# x_train_mean = np.mean(x_train,0)
# x_val_mean = np.mean(x_val,0)

# x_train_zero_centered = x_train - x_train_mean
# x_val_zero_centered = x_val - x_val_mean

# ------------------------------------------------------------------------------------------------------#

# train_PCA = cd.pca_zm_proj(x_train_zero_centered,K=10)
# val_PCA = cd.pca_zm_proj(x_val_zero_centered,K=10)

# train_PCA_vector = np.dot(x_train_zero_centered,train_PCA)
# val_PCA_vector = np.dot(x_val_zero_centered,val_PCA)

# weight_fitted,bias,error = fit_linreg(train_PCA_vector,y_train)

# print("Train error: ",error)

# compute_error_leastSquares = compute_error(train_PCA_vector,y_train,weight_fitted,bias)

# print("Validation error: ",compute_error_leastSquares)

# ------------------------------------------------------------------------------------------------------#

# train_PCA = cd.pca_zm_proj(x_train_zero_centered,K=100)
# val_PCA = cd.pca_zm_proj(x_val_zero_centered,K=100)

# train_PCA_vector = np.dot(x_train_zero_centered,train_PCA)
# val_PCA_vector = np.dot(x_val_zero_centered,val_PCA)

# weight_fitted,bias,error = fit_linreg(train_PCA_vector,y_train)

# print("Train error: ",error)

# compute_error_leastSquares = compute_error(train_PCA_vector,y_train,weight_fitted,bias)

# print("Validation error: ",compute_error_leastSquares)


# ------------------------------------------------------------------------------------------------------#

#Q3b1 ---------------------------------------------

# fig1, ax1 = plt.subplots()
# ax1.hist(k_feature(x_train,46))

# plt.hist(k_feature(x_train,4))


# plt.hist(k_feature(x_train,30))
# plt.show()



# plt.hist(k_feature(train_PCA_vector,65))
# plt.show()

# #Q3b2 ---------------------------------------------

# aug_fn = lambda X: np.concatenate([X, X==0, X<0], axis=1) # Python

# aug_train = aug_fn(x_train)
# aug_val = aug_fn(x_val)

# weight_fitted,bias,error = fit_linreg(aug_train,y_train)

# print("Train error: ",error)

# compute_error_leastSquares = compute_error(aug_val,y_val,weight_fitted,bias);
# print("Validation error: ",compute_error_leastSquares)


# Q4   ------------------------------------------------------------------------------------------------------#

# K = 10 # number of thresholded classification problems to fit
# mx = np.max(y_train); mn = np.min(y_train); hh = (mx-mn)/(K+1)
# thresholds = np.linspace(mn+hh, mx-hh, num=K, endpoint=True)
# x_train_regression,x_valid_regression = np.empty([x_train.shape[0],10]),np.empty([x_val.shape[0],10])
# ww_sigmoidal = np.empty([x_train.shape[1],10])
# bias_sigmoidal = np.empty(10,)
# log_regression_list = []
# for kk in range(K):
# 	labels = y_train > thresholds[kk]
# 	ww,b = cd.fit_logreg_gradopt(x_train,labels,alpha=10)
# 	ww_sigmoidal[:,kk]=ww;bias_sigmoidal[kk]=b
# 	x_train_regression[:,kk] = sigmoid_function(x_train,ww,b)
# 	x_valid_regression[:,kk] = sigmoid_function(x_val,ww,b)

# # ... fit logistic regression to these labels


# weight_fitted,bias,error = fit_linreg(x_train_regression,y_train)

# print("Train error: ",error)

# compute_error_leastSquares = compute_error(x_valid_regression,y_val,weight_fitted,bias);

# print("Validation error: ",compute_error_leastSquares)


# Q5   ------------------------------------------------------------------------------------------------------#
# print(weight_fitted.shape)
# print(ww_sigmoidal.shape)

# ww,bb,w1,b1 = cd.fit_neural_network_gradopt((weight_fitted,bias,ww_sigmoidal.T,bias_sigmoidal),x_train,y_train,alpha=10)

# y_pred = cd.nn_cost((ww,bb,w1,b1),x_train)
# error_train = sqrt(((y_pred-y_train)**2).mean())


# y_pred = cd.nn_cost((ww,bb,w1,b1),x_val)
# error_val = sqrt(((y_pred-y_val)**2).mean())

# print("Networks init from Q4 fit results : ")
# print(error_train,error_val)



# 373 -> 100 
random_ww_sigmoidal1 =  np.random.uniform(-1,1,(373,50))*0.01
random_bias_sigmoidal1 =  np.zeros(50)

#100 -> 10 
random_ww_sigmoidal2 =  np.random.uniform(-1,1,(50,10))*0.01
random_bias_sigmoidal2 =  np.zeros(10)

#10 -> 1 
random_weight =  np.random.uniform(-1,1,10)*0.01
random_bias =  0


ww,bb,w2,b2,w1,b1 = cd.fit_neural_network_gradopt((random_weight,random_bias,random_ww_sigmoidal2.T,random_bias_sigmoidal2,random_ww_sigmoidal1.T,random_bias_sigmoidal1),x_train,y_train,alpha=10)

y_pred = cd.nn_cost((ww,bb,w2,b2,w1,b1),x_train)
error_train = sqrt(((y_pred-y_train)**2).mean())

print("Networks randomly init were fit results : ")
print(error_train,error_val)

# final_Error_random_params = cd.fit_neural_network_gradopt((random_weight,random_bias,random_ww_sigmoidal.T,random_bias_sigmoidal), x_train, y_train,alpha=10)
