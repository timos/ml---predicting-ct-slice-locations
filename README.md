# ML - Predicting CT Slice Locations

In this project we explored various aspects of Machine Learning and Pattern Recignition and how they affect the performance of different techniques. We investigated:

* PCA - Dimensionality Reduction.
* Data Augmentation.
* Logistic Regression as a way for to initialize a network's parameters.
* Error Bars and limitations.

### Build With

* [Python 3.7](https://www.python.org/downloads/release/python-370/)
* [Numpy](http://www.numpy.org/)
* [Matplotlib](https://matplotlib.org/)
* [Scipy](https://www.scipy.org/)

